<?php
do_action('pyxl_before_footer');
?>
    <footer id="siteFooter" class="site-footer">
        <div class="container">
            <?php get_search_form(); ?>
            <p class="site-footer__copyright">
                <?php echo wp_kses_post(sprintf('%s | All Rights Reserved &copy; %s', get_bloginfo('name'), date('Y'))); ?>
            </p>
        </div>
    </footer><!-- #siteFooter -->
<?php
do_action('pyxl_after_footer');
wp_footer();
