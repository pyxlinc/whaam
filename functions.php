<?php
/**
 * @package Pyxl
 * @author  Pyxl Inc. <development@pyxl.com>
 * @license GPLv3+
 * @version 2.2.0
 */

namespace Pyxl\Theme;

// Setting PATH with dirname and concatenating '/' allows paths to be analysed via an IDE like PHPStorm
define(__NAMESPACE__.'\PATH', dirname(__FILE__).'/');
define(__NAMESPACE__.'\URI', trailingslashit(get_template_directory_uri()));
define(__NAMESPACE__.'\VERSION', '2.2.0');

require_once 'lib/autoload.php';

add_action('after_setup_theme', function () {
    View::init();
    Setup::init();
    MarkupHelper::init();
    Enqueues::init();
    DevEnvHelpers::init();
    Menu\Social::init();
});
