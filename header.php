<?php

use Pyxl\Theme\SVG;

get_template_part('head');
get_template_part('template-parts/top-bar');

do_action('pyxl_before_header');
?>
    <header id="siteHeader" class="site-header">
        <div class="container">
            <div id="siteLogo" class="site-header__logo">
                <a href="<?php echo esc_url(get_home_url()); ?>">
                    <?php echo SVG::get('logo'); ?>
                </a>
            </div>
            <?php get_template_part('template-parts/menu-simple'); ?>
            <?php get_template_part('template-parts/menu-social'); ?>
        </div>
    </header><!-- #siteHeader -->
<?php
do_action('pyxl_after_header');