<?php
do_action('pyxl_before_head');
?>
    <head>
        <!-- Prefetch -->
        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="//use.typekit.net" rel="dns-prefetch">
        <!-- Meta Tags -->
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php
        get_template_part('template-parts/analytics', 'google');
        wp_head();
        ?>
    </head>
<?php
do_action('pyxl_after_head');
