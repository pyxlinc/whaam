<?php

use Pyxl\Theme\NavWalker;

?>
<nav class="navbar navbar-expand-md navbar-dark bg-dark bg-faded">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#BootstrapNavWalker" aria-controls="BootstrapNavWalker" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
            <?php bloginfo('name'); ?>
        </a>
        <?php
        wp_nav_menu([
            'menu'            => 'menu_primary',
            'theme_location'  => 'menu_primary',
            'container'       => 'div',
            'container_id'    => 'bootstrapNav',
            'container_class' => 'collapse navbar-collapse',
            'menu_id'         => false,
            'menu_class'      => 'navbar-nav mr-auto',
            'depth'           => 2,
            'fallback_cb'     => 'BootstrapNavWalker::fallback',
            'walker'          => new NavWalker\Bootstrap(),
        ]);
        ?>
    </div>
</nav>