<?php

use Pyxl\Theme\NavWalker;

?>
<nav id="siteNavigation" class="site-navigation">
    <button id="mobileNavButton"
            class="site-navigation__button"
            data-target="#siteNavigation"
            aria-controls="siteNavigation"
            aria-expanded="false"
            type="button" data-toggle="collapse"
            aria-label="Toggle navigation">
        <svg class='icon'>
            <use xlink:href='#icon-bars'/>
        </svg>
    </button>
    <?php
    wp_nav_menu([
        'menu'            => 'menu_primary',
        'theme_location'  => 'menu_primary',
        'container'       => false,
        'container_id'    => '',
        'container_class' => '',
        'menu_id'         => false,
        'menu_class'      => 'site-navigation__menu',
        'depth'           => 2,
        'walker'          => new NavWalker\Simple(),
    ]);
    ?>
</nav>