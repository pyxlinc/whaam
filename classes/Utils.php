<?php

namespace Pyxl\Theme;

use const Pyxl\Theme\PATH;
use const Pyxl\Theme\URI;

class Utils
{
    /**
     * @param $file
     * @return bool|int|null
     * @since 1.9.0
     */
    public static function versionCacheBuster($file)
    {
        $theme_path = PATH;

        if (!file_exists($theme_path . $file)) {
            return null;
        }
        return filemtime(PATH . $file);
    }

    /**
     * @param $path
     * @return null|string
     */
    public static function fileEnvCheck($path)
    {
        // Our default variable, will pass as null by default.
        $qualifiedFile = null;

        $themePath = PATH;
        $themeUri  = URI;

        // If dev file exists e.g( app.js ) override $qualifiedFile.
        if (file_exists($themePath . $path)) {
            $qualifiedFile = $themeUri . $path;
        }

        // Create a string to match a possible production file e.g.( app.min.js ) which is likely uglified/minified.
        $extensionPos   = strrpos($path, '.');
        $fileProduction = substr($path, 0, $extensionPos) . '.min' . substr($path, $extensionPos);

        // Test for production file e.g.( app.min.js) override $qualifiedFile.
        if (file_exists($themePath . $fileProduction)) {
            $qualifiedFile = $themeUri . $fileProduction;
        }

        // In order or priority return null, development file, or production file.
        return $qualifiedFile;
    }

    /**
     *
     * Module Views
     *
     * @param string $name name of view to render
     * @example HTML Return file located at /$base_path/$name.php
     * @return void;
     *
     **/
    public static function moduleInclude($name)
    {
        $base_path = PATH . 'modules';
        $filename  = $base_path . '/' . $name . '.php';
        do_action('pyxl_module_before');
        if (file_exists($filename)) {
            do_action('before_frontend_' . $name);
            include($filename);
            do_action('after_frontend_' . $name);
        }
        do_action('pyxl_module_after');
    }
}