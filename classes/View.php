<?php

namespace Pyxl\Theme;

use Timber\Timber;

class View
{
    private $path;
    private $type;
    private $file;
    private $context = [];
    private $queriedObject;

    public static function init()
    {
        $class = new self;
        add_action('view', [$class, 'run']);
    }

    public function run($queriedObject)
    {
        $this->queriedObject = $queriedObject;
        $this->type          = $this->getType();
        $this->path          = $this->getPath($this->type);
        $this->context       = $this->setContext();
        // var_dump($this);
        $this->render($this->path, $this->context);
    }

    private function getType()
    {
        // Archive, Single, Search, 404
        if (is_archive() || is_home()) {
            $type = 'archives';
        } elseif (is_singular()) {
            $type = 'singles';
        } elseif (is_search()) {
            $type = 'search';
        } else {
            $type = '404'; // Default
        }

        return $type;
    }

    private function getPath($type)
    {
        $path = (object)[
            'dir'      => 'views/',
            'basename' => '',
            'file'     => '',
        ];

        switch ($type) {
            case 'archives':
                if (is_home()) {
                    $path->basename = 'post';
                    $path->dir      .= "{$type}/post_type/{$path->basename}";
                } elseif (is_post_type_archive()) {
                    $path->basename = $this->queriedObject->name;
                    $path->dir      .= "{$type}/post_type/{$path->basename}";
                } else {
                    $path->basename = $this->queriedObject->taxonomy;
                    $path->dir      .= "{$type}/taxonomy/{$path->basename}";

                }
                break;
            case 'singles':
                $path->basename = $this->queriedObject->post_type;
                $path->dir      .= "{$type}/{$path->basename}";
                break;
            case 'search':
                $path->basename = 'search';
                $path->dir      .= "{$path->basename}";
                break;
            default:
                $path->basename = '404';
                $path->dir      .= "{$path->basename}";
                break;
        }

        if (class_exists(Timber::class)) {
            $path->file = PATH."{$path->dir}/{$path->basename}.twig";
        } else {
            $path->file = PATH."{$path->dir}/{$path->basename}.php";
        }

        return $path;
    }

    private function setContext()
    {
        $context = [
            'filter'         => $this->path->dir,
            'file'           => $this->path->file,
            'queried_object' => $this->queriedObject,
        ];

        return $context;
    }

    private function render($path, $data = [])
    {
        $data = apply_filters($path->dir, $data);
        do_action('pyxl_before_render', $data);

        if (class_exists(Timber::class)) {
            Timber::render($path->file, $data);
        } else {
            include $path->file;
        }
    }

    private function getArchiveType()
    {
        if (is_post_type_archive() || is_home()) {
            $type = 'post_type';
        } else {
            $type = 'taxonomy';
        }
        return $type;
    }
}