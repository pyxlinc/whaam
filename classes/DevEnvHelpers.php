<?php

namespace Pyxl\Theme;

class DevEnvHelpers
{

    public static function init()
    {
        $class = new self;
        add_action('pyxl_module_after', [$class, 'displayFilename']);
    }

    /**
     *
     * Shows the filename of the view if WP_DEBUG is true
     *
     * @example HTML Returns the filename being displayed in an html comment
     *
     **/
    public function displayFilename()
    {
        if (!WP_DEBUG) {
            return false;
        }
        echo '<!-- ' . __FILE__ . ' -->';
    }

}